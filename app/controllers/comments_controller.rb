class CommentsController < ApplicationController
    before_action :set_question
    
    def create
        @comments = @question.comments.create! comments_params
        @comments.update(:user_id => current_user.id)
        redirect_to @question
    end
    
    private
        def set_question
            @question = Question.find(params[:question_id])
        end
        
        def comments_params
            params.required(:comment).permit(:content) 
        end
end